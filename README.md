vCard Handler
=============

Since the iPhone can't handle vCard files. This solution will automatically detect the iPhone and 
ask for the email address of the requester and send the vCard as an attachment.

Written fully in [Cloud 9 IDE](http://cloud9ide.com) and code heavily borrowed from [Neil Crosby](https://github.com/NeilCrosby/multi-level-vcards)

Setup
-----

To setup change the file name config-sample.php to config.php and update the information to your setup. Update the URL in .htaccess to your setup and you are ready to test.