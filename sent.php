<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Get Mike Wills' vCard Sent</title>
        <meta name="viewport" content="width=320" />
    </head>
    <body>
        <p>Check your inbox. The vCard has been sent.</p>
    </body>
</html>