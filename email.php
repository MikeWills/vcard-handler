<?php
// Adapted from https://github.com/NeilCrosby/multi-level-vcards

function is_valid_email_address() {
    return (
        isset($_GET['email']) &&
        filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)
    );
}

// first, check that a valid email address has been given
if (is_valid_email_address()) {

    $email = $_GET['email'];
    
    require 'config.php';
    
    // now, get a copy of whatever version of the vCard the requester is allowed
    $url = $rootUrl . $vcardFile;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $contents = curl_exec($ch);

    // close cURL resource, and free up system resources
    curl_close($ch);
    
    #$contents = file_get_contents('../vcard.vcf');

    // and save it to a temporary location (so that it gets a nice filename)
    $dir = '/tmp/'.md5(time());
    mkdir($dir);
    $tempFile = $dir.'/MikeWills.vcf';
    file_put_contents($tempFile, $contents);

    // send the email
    require 'geekMail-1.0.php';
 
    $geekMail = new geekMail(); 
    $geekMail->setMailType('text');
 
    $geekMail->from($fromEmail, $fromName);
    $geekMail->to($email);
 
    $geekMail->subject($subject); 
    $geekMail->message("Thanks for downloading my vCard.\n\nIf you're using an iPhone, scroll down to the bottom of the attached card to add it to your address book.\n\nDon't forget you can always get an up to date copy from http://v.mikewills.me");
 
    $geekMail->attach($tempFile);
 

    if ($geekMail->send()) {
        unlink($tempFile);
        header("Location: " . $rootUrl . "sent.php");
    } else {
        $errors = $geekMail->getDebugger();
        print_r($errors);
    }

    unlink($tempFile);
    exit();
}
?>

<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Get Mike Wills' vCard via Email</title>
        <meta name="viewport" content="width=320" />
    </head>
    <body>
        <p>Because the iPhone cannot automatically handle vCard files in Safari, you must be emailed the vCard to 
        have it added to your phone. I do not store nor log your email address.</p>
        <form method="get" action="">
            <p>
                <label for="email">Please enter your email address then press submit.</label>
                <input type="text" id="email" name="email">
            </p>
            <p>
                <input type="submit">
            </p>
        </form>
    </body>
</html>